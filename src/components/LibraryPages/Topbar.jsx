import React, { Component } from "react";

class Topbar extends Component {
  render() {
    return (
      <div className="row mx-0 bg-primary py-2">
        <div className="col-12 col-md-4 my-auto px-4">
          <form className="d-block">
            <input
              className="search-input border-0 px-4 py-1"
              type="text"
              placeholder="Search solution.."
              name="search"
            />
            <button
              className="search-input-button border-0 px-2 py-1"
              type="submit"
            >
              <i className="fa fa-search"></i>
            </button>
          </form>
        </div>
        <div className="col-12 col-md-8 text-right text-white pr-5">
          <div className="d-inline">
            <small className="mr-4">
              <u>30 Days left for your trial account</u>
            </small>
            <button className="btn btn-upgrade text-white pt-0 pb-1 mr-4">
              <small>Upgrade My Plan</small>
            </button>
          </div>
          <div className="d-inline align-middle">
            <small className="d-inline-block mb-0" width="10%">
              <span className="d-block text-left" style={{ fontSize: "9px" }}>
                used
              </span>
              <span className="d-block" style={{ fontSize: "11px" }}>
                30Kb
              </span>
            </small>
          </div>
          <div className="d-inline-block mx-2">
            <div className="progress">
              <div
                className="progress-bar bg-warning"
                role="progressbar"
                style={{ width: "30%" }}
                aria-valuenow="10"
                aria-valuemin="0"
                aria-valuemax="100"
              ></div>
            </div>
          </div>
          <div className="d-inline align-middle mr-4">
            <small className="d-inline-block mb-0" width="10%">
              <span className="d-block text-left" style={{ fontSize: "9px" }}>
                total
              </span>
              <span className="d-block" style={{ fontSize: "11px" }}>
                100Kb
              </span>
            </small>
          </div>
          <p className="d-inline mr-4">
            <i className="fas fa-th"></i>
          </p>
          <p className="d-inline">
            <i className="far fa-bell"></i>
          </p>
        </div>
      </div>
    );
  }
}

export default Topbar;

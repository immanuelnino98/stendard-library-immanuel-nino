import React, { Component } from "react";
import Files from "./Files";

import Sidebar from "./Sidebar";
import Topbar from "./Topbar";

class LibraryLayout extends Component {
  render() {
    return (
      <div className="row mx-0">
        <Sidebar />
        <div className="col-10 px-0">
          <Topbar />
          <Files />
        </div>
      </div>
    );
  }
}

export default LibraryLayout;

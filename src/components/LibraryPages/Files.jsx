import React, { Component } from "react";
import classnames from "classnames";

import FilesList from "./FilesList";
import Filter from "./Filter";
import FileDetails from "./FileDetails";

class Files extends Component {
  constructor() {
    super();
    this.state = {
      showFilter: true,
      selectedData: {},
    };
  }

  onClickFilter() {
    if (this.state.showFilter) {
      document.getElementById("filter-bar").style.height = "197px";
    } else {
      document.getElementById("filter-bar").style.height = "0px";
    }
    this.setState({ showFilter: !this.state.showFilter });
  }

  getSelectedData(event, data) {
    event.preventDefault();
    this.setState({ selectedData: data });
  }

  render() {
    const { showFilter } = this.state;
    return (
      <>
        <div id="filter-bar" className="filter-bar">
          <Filter
            showFilter={showFilter}
            onClickFilter={this.onClickFilter.bind(this)}
          />
        </div>

        <div className="row mx-0">
          <div className="col-9 p-4">
            <button
              className={classnames("btn btn-light text-secondary py-0", {
                "d-none": !showFilter,
              })}
              onClick={() => this.onClickFilter()}
            >
              <small className="font-weight-bolder">
                <i className="fas fa-filter mr-2"></i>Filter
              </small>
            </button>
            <FilesList
              getSelectedData={(event, data) =>
                this.getSelectedData(event, data)
              }
            />
          </div>
          <FileDetails data={this.state.selectedData} />
        </div>
      </>
    );
  }
}

export default Files;

import React, { Component } from "react";
import Logo from "../../assets/img/logo.png";
import isEmpty from "../../utils/is-empty";

import FolderMenu from "./../data/folder_tree_left_side_bar.json";

class Sidebar extends Component {
  render() {
    return (
      <div className="col-2 bg-light px-0">
        <div className="text-center my-4">
          <img className="d-block w-50 mx-auto" src={Logo} alt={Logo} />
          <button className="btn bg-white shadow-sm text-primary mt-3 py-1">
            <small className="font-weight-bold mb-0 px-3">+ Add</small>
          </button>
        </div>
        <div className="px-0 py-4 sidebar-menu">
          {FolderMenu.map((data, key) => {
            return (
              <div key={key}>
                <small>
                  {!isEmpty(data.children) ? (
                    <i className="fas fa-caret-right mr-1"></i>
                  ) : (
                    <span className="mr-2"></span>
                  )}
                  <i className="far fa-folder mr-2"></i>
                  {data.id}
                </small>
                {!isEmpty(data.children)
                  ? data.children.map((child, childKey) => {
                      return (
                        <small key={childKey} className="sidebar-submenu">
                          <i className="far  fa-folder mr-2"></i>
                          {child.text}
                        </small>
                      );
                    })
                  : ""}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Sidebar;

import React, { Component } from "react";
import classnames from "classnames";

class Filter extends Component {
  render() {
    const { showFilter, onClickFilter } = this.props;
    return (
      <div
        className={classnames("row mx-0 px-4 pt-4")}
        style={{ backgroundColor: "#e7f3ff" }}
      >
        <button className="btn btn-light text-secondary py-0">
          <small className="font-weight-bolder" onClick={onClickFilter}>
            <i className="fas fa-filter mr-2"></i>Filter
          </small>
        </button>
        <div className="w-100 mt-3"></div>

        <div className="col-2 px-0 mr-4">
          <div className="form-group filter-form">
            <label className="mb-0">Document Type</label>
            <select className="form-control form-control-sm border-0 rounded">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
        <div className="col-2 px-0 mr-4">
          <div className="form-group filter-form">
            <label className="mb-0">Format</label>
            <select className="form-control form-control-sm border-0 rounded">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
        <div className="col-2 px-0 mr-4">
          <div className="form-group filter-form">
            <label className="mb-0">Author</label>
            <select className="form-control form-control-sm border-0 rounded">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
        <div className="col-2 px-0 mr-4">
          <div className="form-group filter-form">
            <label className="mb-0">Department</label>
            <select className="form-control form-control-sm border-0 rounded">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
        <div className="w-100"></div>
        <div className="col-2 px-0 mr-4">
          <div className="form-group filter-form">
            <label className="mb-0">Select Category</label>
            <select className="form-control form-control-sm border-0 rounded">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
        <div className="col-2 px-0 mr-4">
          <div className="form-group filter-form">
            <label className="mb-0">File Status</label>
            <select className="form-control form-control-sm border-0 rounded">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
        <div className="col-3 px-0 mr-4 mt-auto mb-3">
          <button
            className="btn text-white pt-0 pb-1 mr-2"
            style={{ backgroundColor: "#1890ff" }}
            onClick={onClickFilter}
          >
            <small>
              <i className="fas fa-times mr-2"></i>Close
            </small>
          </button>
          <button
            className="btn btn-light pt-0 pb-1"
            style={{ color: "#1890ff" }}
          >
            <small>
              <i className="fas fa-undo mr-2"></i>Reset Filter
            </small>
          </button>
        </div>
      </div>
    );
  }
}

export default Filter;

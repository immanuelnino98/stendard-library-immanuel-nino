import React, { Component } from "react";
import isEmpty from "../../utils/is-empty";

class FileDetails extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="col-3 bg-light px-3 py-5">
        <h6>File Details</h6>
        <div className="file-detail mt-2">
          <small>File Name</small>
          <p>{!isEmpty(data.file) ? data.file : "-"}</p>
        </div>
        <div className="file-detail mt-2">
          <small>Doc no</small>
          <p>{!isEmpty(data.doc_no) ? data.doc_no : "-"}</p>
        </div>
        <div className="file-detail mt-2">
          <small>File Type</small>
          <p>{!isEmpty(data.file_type) ? data.file_type : data.type}</p>
        </div>
        <div className="file-detail mt-2">
          <small>Size</small>
          <p>{!isEmpty(data.size) ? data.size : "-"}</p>
        </div>
        <div className="file-detail mt-2">
          <small>Last Updated</small>
          <p>{!isEmpty(data.last_updated) ? data.last_updated : "-"}</p>
        </div>
        <div className="file-detail mt-2">
          <small>File Status</small>
          <p>{!isEmpty(data.file_status) ? data.file_status : "-"}</p>
        </div>
      </div>
    );
  }
}

export default FileDetails;

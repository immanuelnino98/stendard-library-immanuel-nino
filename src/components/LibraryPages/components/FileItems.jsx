import React, { Component } from "react";
import classnames from "classnames";

import Docs from "../../../assets/img/docs.png";
import JPG from "../../../assets/img/837f8a48-175a-4656-b802-2596aa69ec22.svg";
import Folder from "../../../assets/img/folder.png";

import { Menu, Item, Separator, useContextMenu } from "react-contexify";

import "react-contexify/dist/ReactContexify.css";

const MENU_ID = "menu-id";

export default function FileItems(props) {
  function renderImage(type) {
    switch (type) {
      case "docx":
        return Docs;
      case "jpg":
        return JPG;
      case "":
        return Folder;
      default:
        break;
    }
  }
  const { data, selectedId } = props;

  const { show } = useContextMenu({
    id: MENU_ID,
  });

  return (
    <>
      <li
        className={classnames(
          "text-center file-items d-block mr-2 p-2 rounded mb-2",
          { "active-item": data.id === selectedId }
        )}
        onClick={(event) => props.onClickItem(event, data)}
        onContextMenu={show}
      >
        <div className="file-items-image">
          <img
            className=""
            width="60px"
            src={renderImage(data.file_type)}
            alt=""
          />
        </div>
        <small>{data.file}</small>
      </li>
      <div>
        <Menu id={MENU_ID}>
          <Item>
            <i class="far fa-eye mr-2"></i>Preview
          </Item>
          <Item>
            <span className="text-success">
              <i class="far fa-edit mr-2"></i>Live Edit
            </span>
          </Item>
          <Item>
            <i class="far fa-file mr-2"></i>Edit in Word 265
          </Item>
          <Item>
            <i class="fas fa-italic mr-2"></i>Rename
          </Item>
          <Item>
            <i class="fas fa-link mr-2"></i>Copy Link
          </Item>
          <Item>
            <i class="fas fa-arrows-alt mr-2"></i>Move File
          </Item>
          <Item>
            <i class="fas fa-list mr-2"></i>Start Version Control
          </Item>
          <Item>
            <i class="fas fa-exchange-alt mr-2"></i>See Workflow
          </Item>
          <Item>
            <i class="fas fa-share-alt mr-2"></i>Share
          </Item>
          <Separator />
          <Item>
            <i class="fas fa-info mr-2"></i>Document Info
          </Item>
          <Item>
            <i class="fas fa-download mr-2"></i>Download
          </Item>
          <Separator />
          <Item>
            <span className="text-danger">
              <i class="far fa-trash-alt mr-2"></i>Delete File
            </span>
          </Item>
        </Menu>
      </div>
    </>
  );
}

// import React, { Component } from "react";
// import classnames from "classnames";

// import Docs from "../../../assets/img/docs.png";
// import JPG from "../../../assets/img/837f8a48-175a-4656-b802-2596aa69ec22.svg";
// import Folder from "../../../assets/img/folder.png";

// class FileItems extends Component {
//   renderImage(type) {
//     switch (type) {
//       case "docx":
//         return Docs;
//       case "jpg":
//         return JPG;
//       case "":
//         return Folder;
//       default:
//         break;
//     }
//   }

//   render() {
//     const { data, selectedId } = this.props;
//     return (
//       <li
//         className={classnames(
//           "text-center file-items d-block mr-2 p-2 rounded mb-2",
//           { "active-item": data.id === selectedId }
//         )}
//         onClick={(event) => this.props.onClickItem(event, data)}
//       >
//         <div className="file-items-image">
//           <img
//             className=""
//             width="60px"
//             src={this.renderImage(data.file_type)}
//             alt=""
//           />
//         </div>
//         <small>{data.file}</small>
//       </li>
//     );
//   }
// }
// export default FileItems;

import React, { Component, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Docs from "../../assets/img/docs.png";
import JPG from "../../assets/img/837f8a48-175a-4656-b802-2596aa69ec22.svg";
import Folder from "../../assets/img/folder.png";
import FileItems from "./components/FileItems";

import DataFile from "../data/default_library.json";
import FileTable from "./FileTable";

class FilesList extends Component {
  constructor() {
    super();
    this.state = {
      selectedData: {},
      files: DataFile,
    };
  }

  onClickItem(event, data) {
    event.preventDefault();
    this.setState({ selectedData: data });
    this.props.getSelectedData(event, data);
  }

  renderImage(type) {
    switch (type) {
      case "docx":
        return Docs;
      case "jpg":
        return JPG;
      case "":
        return Folder;
      default:
        break;
    }
  }

  handleOnDragEnd(result) {
    console.log("destination", result.destination, result.combine);

    if (
      result.combine == null &&
      result.destination != null &&
      result.destination.droppableId != "files"
    ) {
      //just removing the dragging item
      const combineTaskIds = Array.from(this.state.files); //create new array with current columns taskids
      combineTaskIds.splice(result.source.index, 1); // from this index we want to remove 1 item

      this.setState({ files: combineTaskIds });
      toast(
        <div>
          <small className="text-dark">
            <b>Succesfully move the file to</b>
          </small>
          <br />
          <img className="mr-1" width="15px" src={Folder} alt="" />
          <small>{result.destination.droppableId}</small>
        </div>
      );
    }

    if (!result.destination) return;

    const items = Array.from(this.state.files);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    this.setState({ files: items });
  }

  render() {
    return (
      <div>
        <DragDropContext onDragEnd={this.handleOnDragEnd.bind(this)}>
          <div className="d-block mt-3">
            <h6>Recent Files</h6>
            <Droppable droppableId="files" direction="horizontal">
              {(provided) => (
                <ul
                  className="d-block library-list p-0"
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                >
                  {this.state.files.map((data, index) => {
                    if (data.type === "folder") {
                      return null;
                    }
                    return (
                      <Draggable
                        key={data.file}
                        draggableId={data.file}
                        index={index}
                      >
                        {(provided) => (
                          <div
                            className="library-item align-top"
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <FileItems
                              data={data}
                              selectedId={this.state.selectedData.id}
                              onClickItem={(event, data) =>
                                this.onClickItem(event, data)
                              }
                            />
                          </div>
                        )}
                      </Draggable>
                    );
                  })}
                  {provided.placeholder}
                </ul>
              )}
            </Droppable>
          </div>
          <div className="d-block mt-4">
            <h6>Files</h6>
            <ul className="d-block library-list p-0">
              {this.state.files.map((data, index) => {
                if (data.type === "file") {
                  return null;
                }
                return (
                  <Droppable
                    droppableId={data.file}
                    direction="horizontal"
                    isCombineEnabled={true}
                    key={index}
                  >
                    {(provided) => (
                      <div
                        className="library-item align-top"
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                      >
                        <FileItems
                          data={data}
                          selectedId={this.state.selectedData.id}
                          onClickItem={(event, data) =>
                            this.onClickItem(event, data)
                          }
                        />
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                );
              })}
            </ul>
          </div>
        </DragDropContext>

        <FileTable data={DataFile} />
        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
    );
  }
}

export default FilesList;

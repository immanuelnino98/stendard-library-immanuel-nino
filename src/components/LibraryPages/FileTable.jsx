import React from "react";
import Docs from "../../assets/img/docs.png";
import JPG from "../../assets/img/837f8a48-175a-4656-b802-2596aa69ec22.svg";
import Folder from "../../assets/img/folder.png";

import {
  Menu,
  Item,
  Separator,
  Submenu,
  useContextMenu,
} from "react-contexify";
import "react-contexify/dist/ReactContexify.css";

const MENU_ID = "menu-id";

export default function FileTable(props) {
  function renderImage(type) {
    switch (type) {
      case "docx":
        return Docs;
      case "jpg":
        return JPG;
      case "":
        return Folder;
      default:
        break;
    }
  }

  const { show } = useContextMenu({
    id: MENU_ID,
  });

  const DataFile = props.data;
  return (
    <table className="table table-borderless library-table">
      <thead className="border-bottom">
        <tr>
          <th scope="col" width="40%">
            Name
          </th>
          <th scope="col">Doc No</th>
          <th scope="col">Size</th>
          <th scope="col">Status</th>
          <th scope="col">Last Updated</th>
        </tr>
      </thead>
      <tbody>
        {DataFile.map((data, key) => {
          if (data.type === "folder") {
            return null;
          }
          return (
            <tr key={data.id} onContextMenu={show}>
              <td>
                <img
                  className="mr-1"
                  width="15px"
                  src={renderImage(data.file_type)}
                  alt=""
                />
                {data.filename}
              </td>
              <td>{data.doc_no}</td>
              <td>{data.size}</td>
              <td>{data.file_status}</td>
              <td>{data.last_updated}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

import React, { Component } from "react";
import classnames from "classnames";
import isEmpty from "../../utils/is-empty";

import LoginPeople from "../../assets/img/working.png";

class LoginLayout extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: "",
      alert: {},
    };
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onLogin = (e) => {
    e.preventDefault();

    const { email, password } = this.state;

    let invalid = false;
    // this.setState({ errors: {} });

    if (email !== "immanuel@mail.com" || password !== "pass123") {
      this.setState({
        errors:
          "Email or password are incorrect, or verify your email first if you just registered your account",
      });
      this.setState({ alert: "invalid" });
      invalid = true;
    }

    if (isEmpty(password)) {
      this.setState({
        errors: "Please input your password first to login into your account",
      });
      this.setState({ alert: "password" });
      invalid = true;
    }

    if (isEmpty(email)) {
      this.setState({
        errors: "Please input your email first to login into your account",
      });
      this.setState({ alert: "email" });
      invalid = true;
    }

    if (!invalid) {
      window.location.href = "/library";
    }
  };

  render() {
    const { email, password, errors, alert } = this.state;
    console.log(alert);
    return (
      <div className="login-page">
        <div className="px-5">
          <img
            width="250px"
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdEAAABsCAMAAAACPni2AAAAilBMVEX///9Hg8RTvZE5fMEud79BgMMzecA9fsI3e8FDgcPs8fiZtdr1+PtUiscsdr/M2eylvt56odG1yeRNhsXT3+7N2uzo7vaLrNZtmc3D0+nc5fKDp9SuxOGcuNtbjsm7zeY/uIdhkspznc/d8Ofx+fUbcLyj2MC138yW07jq9vGt3Mei2MCO0LNYvpQJqM+UAAARsklEQVR4nO1dibKrNhI1ASEEGO8rXp49yUwymfn/3xuWbm1IINu6Ny81OpWq1LtGaDlSq9XqbmazgICAgICAgICAgICAgICAgICAgICAgICAgL8S++1usT2Xf3UzArzgfL0ladYiiVf3EVb35x7f17SAN7DepJRFAEZotbIyVlQd7799Z/P84Leu4dXir26Hjks/opXHV55oHGkgycqyTgvSPZB4rP+bkHYNz346Rte0W0extxeWx4zphHacnoyP/30ZTX5qRiNvjJ5jYuCzI+1gej4w6hueGV1UpgXaI14ZCgRGfcMvo4tKiFmaJUmapLGgOK6HJQKjvuGV0TMnlCbP+775S3m+PBIuh7ProEhg1Dd8MlriemRpnYs/n48ZUlrt9DKBUd/wyegRFiOLt+oP9xSppnqZwKhveGR0kcIOuhmcPc94oqGF9ktg1Dc8MrqBdbgx/HYGtqNEYzsw6hv+GN3Bbpnmpl9PQCnVlKPAqG/4Y7TuuYnX5p9X/c+MqH8OjPqGP0b7Rcgiy895AicY1WgfGPUNb4zue0bji+0BWKR0qfw1MOob3hg99Rcu2X7iAaLaAgOjvuGN0Us8QU0OutFc+Wtg1De8MXqlBr4UwEaqkvcao2We/zReLv8njLKH/Ykqi1uo7gpXR0bzU3GkSdogoY/D3SrbTTjfr/XzeHzWxXo7/bSMcntfXoviurxvB2cyB0a3l8PztmGb2/GwXBgPdQ7VGy8h97v1siiK5eU08A7xxuiyf9HN/sSix6lpzwmxAH2JFBIGfSgvtyQmDG9xGInT6OrmmlSun0lGKWlKE0JplhzXrqt8V2ySLG7LUhpnCalVf6kJRsv1sSnd1tu0mzVvSKKDfT6Vy0uHpeD9fJ131Q8cdsr1Kk27PhEaNw27Fcp7Pe+jQ8OtCesqRuC9DJGg9SE/JPHg1pXR5DEt8PZ1QrWiLK5qhxWeF3FGlKLNPKqe0k3DKKPnuhq0mZGMLC3TqYQRqXCetq5afbFUfXL3rKjqVMBoFhdiJvjWddPpJ3mlNqjvOFQWrwiWzceFaGkpSpJ6Yp02JY1NJNmGO9eMMJo/E3Obm3k4vFDsKgS1EU7rdyJctZTR2M0z05sZrVbIqTdGz70RMHPZqV5g9BSPPMtSm0Nai4Wx8x1IavZ6AiwthHQdfMAKtzO6tM3BbqSpqWqF0fIhu2pJo5EfU6uLCKkKZXC92YyIfrligjujdWL3cumqiwc3roiDvfdtHQZ/CkB+G/gyymBVbyOxMVqOF2+n4bBOmdGtOhPFaNxHJlqDODpLg+uB0Wc/gJnDo66MlnPlQdLoAVmjMMv7G6vMYmx2lMeVtToEVfbF2KaV7/SZwDrlRkJ2bJ+zMHpW+GCtVhW36pX0BhIN9F6JUd1Vix8D6lT5Oxu0K6ouM5+Mwpv0yxXjo0PNKJaAmtFeUgFYnMwPl9NutzhdDq3my38xr7aHmAskTm6HRpdcHh6pNNzErJZfhKtUp1Rn8+Pz+GCN2ilGj7aUmBmV+WCNSr66rhuVfn1dUdkpPdUVdc7oXnj2tNVnaYzUPKQp2jpxbR5NuzZpJjtfpgefjJZoQZg+VuxPcJJZ7OD0Eu8WArDR7DPRg+R4UnbMXZ1yyuhzWMORiqJPadB3KyG5iGmVLsVCYBkpxCmydZjiL2Xx3szoSXKdS29reS3ulxveIVZp6gYyGm857031h5PQyue83Y16tRIH8v2pluZKvPJ5432AOrNXgljsFoY9Vw9YYjhvlEu+3uIBpQeczyx9akXzJ+fM4JsoEZoe9S06L8R0iGG+qYzuOKEseQ6HYTHn66xSf0VG2Zw3/KiQLgiNo8F95Tri741rOHT4YBQXKUvv7oWsVsBSdOFmPj+WdWKh5oQ/sNigii74VEn1sbljwYgaT0ZlzTfZDRsyKvlCRmad/8JbliidQkYjeD1h6nR68HN7Zrx/vnANkkDDvHiOLVGqZA9na5uV0Rv2wXKCa8G5SZU7vBLbQefGw02+4e9WVZQtZ8Ra6Q6rhP/JjHJfyCizatJ7XjdTmqxqPZmmDq9wDcZHy3Etv+GWAG3w46/LZxJL5xc306uNUZSbLLEeTxrkEdSoCDHwpoio1ciMMowo8lowko7Y6zbKEUJmlHc/sV4SS0+Ro/xWhdFKK7/GKZqOqJ11Jr/CE6OlpE3H6cbF9GphdIsSfGJTLkHGMGnGn7HsyD0QFIsSmTr0To3o6Gy8yZRKjF5wSKtR8wWnVJafCqP6JN6j6KhGt7NCodRTlEROpN4ymtHD2ArrmmFmFKfGpOJcDt1GVzhiI/YkvKxl0kK5w4iweOKaZCOdAgWjqEZEyZQaMcfOiQbKjCa6gLgxtxcf5CO4r0im8qbYBBjJ0tXojDUzChc5kSVEUQZ4w0QVrqscZvS4oe8E9EkKCt+Up/aLUrL+C0b5RJo8kGN5InZbidGB6x3OtGw5m8BRWk/+4kcLPTqtOeDX9m3JyCj2j9hNdQLQYb4lwmxQtikDjn0zxdq+us+iLVeJBaN7+NtEvf/4QyrPZ6HE6HCvIAP+rZB3vemnXbE/prr5kaRz2zAZGQVS3K7mkBtcbXO3lQY+4XwDRqHpNIsKLok4o7WDrJ/N/vjxy68zfnSnvC7BaKyP1DpWGzraKTHVPDLavPeQaHeL9psvI6MglWK3cy2sDtJflIMXKZtYonwiIPPgJ+UY7R7xcwowWoKst/tCtmgI/aWjFGU+0i8YHWj9UFU6pZF0OIhTvFM/3LE4NGqRymlinPwmRheZ+6xsAcujN/CjyWRyNsCDFB6EgbM5kOuF+dEbGL24rKR/tIR2lC6hcqxNWBh0+9c2c5yh/Xv4IvXN6Kz3qlDu8SkzCEITo+idPzrd5ZrkJBewHerxNUNA5+H6D4UwmSiF2GgWBtBHR9sMhP7y41fur448cUYHFx2w6ga2fQsKbhp17MhryNfHRNaoDdYCE6O6SJpEP5y92O1VTpf1PZcHFSYCndQnAbAmkVF+TTHSZk7ov2ecKOy3YFSXLbD2R87WCnJuGXUs8DLK9Vy6kHeKCD6/JGdagI9TFxQH7BruY3T0ogDCOm6qejUJpBAYNbuXK1AIRWGKLh+cUd0QDYPhLK9mD69WQDO2D3GHXOnCw8AoXgc5dwLPpN0CodJ6HYdcNRLkuhT4yAGj8C5q34VVQlHsYic5o7pXD8iCyTOyXuBLGZ3NdhE3I+v7lIFR+JM93mIIKNFOF2V3HAWckTqFCpaMk09Nj2vSO+/1jD7AF9m62+mEzo5pWwDVxfI3vO3XGK27ajIXz5AesKi/mNHW6Qco1Y97BkbBvYXN3QGdaI9yLzPaVX2n4g1uQJ/j3mKI/sf851/VpweEzs59ASSQezBrFsid+pgDku9hdHZHa3MyGW3IL32jF9HJvPcYBcXIVaOcxD9/KJQOCf064PT+8orwFlpTeAyMjrvSjaDblN5jFM8Ib4QymPDPH70hAcAJ/Zef14/iSb6JUX4frm7yBkaT6E18wCiY2d0PTKP48wcYEnp8K6H8NP8NVQ1s4y1+EkafstnpU/wODAKl30soipvvYBTsr2pcv4HRVGfq78bof4DCntJvJvQ7GeWiTd6s7PsoS1/Fb63B52eQur9LlH43od8pdTE/jmIWseq67Fnmr6Ll4yPN6JUz8BgEpd9OKBz+jFml/KMfbcV2ajqPkk+a9NHpJXO6suqwJ91JmPTnHbbp/4W/IqX//cVGaNGV2ICRqqRwsmbayZP0j7lbPuAOYSw02yNu/WjLFjq7zejN1AyfWRjcLY+nrIs7ATl96yJ/pegHvkptKxRKwLwtkz6MhelWwP4xo/u/BTAALhf3n6PfrZTKDPTB6DrFLQ7xHqNoBRyxtGtAW2X/L1RHxIyQKTUQCoZkzqjNrlu/Or2hJy7hR6OoDx0mJkbfb2XY7Hcvb7bpPUbRUu+u7PZZENFeArNQNp8ISk17KFzWTDIKhnf37eD6sj3TDEK7ePuJT1K4MYpXgrbsZeN4j1G0nTmnPoHrHtQJ0G9M1uN/H1OK4Lg0yShMb5e7pB5wFf+x9evh9B43qcsVz7fE7puMQjtc7lXl53kbYSAVra+n1EgoXkxPMsqv/x3bBUI3+ljVXTmRMB+OtonRneavOVl53aFf+m8yuh36744CbuW5tyL6hirHwNYaaD62oIfXNKN4vHR0roC1//E26ujU0Y+2olAaFVtwO3W8CblkncSHALU3GeVesW7TCPorqkGxqw7lnz/MhHLfkWlGMcmt2w0p5jF2vyG3ATf6kXRGs9nCsMsXJvPbhTq8DYGRaHBN9y6j3I3fRdYjI9KKPmIog7Lx/G42LHDn92lGcapRp50UnGs8nEYxjmQ0TgVEcyWb2vBiUjW/4T28i6gBsYSdeJdRHv3q4lMKaoPsP7/FSBqHachjzVwYveBUc5BYS9Wf7SOArjh2oENLvTJ/YFi1EDR0ltGD2w3A5Nq4tt5llPvJO5xJr7FhkHGRTi8myfPdgdEZuMg6OJ5iRLKz5+AYUGiNRF4eTd5VaKzRXK4g5/30foDCgS+XtxkVgzolGfDyXuWexwSmE4YnORDKhVFc0ZOGI/5ip51jChglYBdaa6Meji7jmkLCZ9uE7RyDDaMKt6+3GZXibsf96nkkuLZV8GCY8ajAUna3cWGURyeaUoiYXuzJAoiRPLbYLBQ12vdBSsPRvMXVKcabd0Ksq/cZdYzR5oFpg3BGHPnR8rniP+XEKF/9wxQiEnjwuXNcwARyXq1xH+J7hx67hRuwvvtwdXAkD8MZozAkgfQBozx/Q5Tai3MXuGGmlZzf1dvzMGzVb3k6MSqix+nDeoG757FjA5/od4GrKqKGVcqTkAxSfVhPDTySms4tTSxEghDR0Q8YlTJr0IfF+sUTf5kSXO0mc6XwJjP6AqOzFQp0Qi0ia821rcQtFssFXOgQ/ZKvXPFxGLDN17a+YwoFgiWmr0bfCXfrlvWnTxgVyWmaOk2i4cSzErDItFouwqPGlM/ozkRCk/5Q4sioyP0wTNDU4vxwES8vI+fBEGp6pfwqQoQNehNuwEzfl3KhE5LkoeTvmm2LWMS9KbaljxidXQUlNLtqY3cXITyMmcWflOGqafNdzTlGeJMZzU9SrM40ozORoYUkK+2Z3VPkw8ycTfoukFPaZbRe787n7el6E/nemOkwIoJhMy0HchkR8UKabOrl/bRYnC7FMZHy+7UZ3SR8xqhMaUTTTXE6t6yU++3lKZLImT4NB1DSCjYvqJdtLsP7smZSrjdC8tn9NUZLkXQsIllcr7edD05+vtexNBg+V2iLnZzmkLSJNrNYzkFtNtSehKjL0tXytBWdkrMbYh5MNXdnFGtZqD5kdLZW8ki0uRaTtPkvkxNPW/N+dr1RdNku8biWN5Temia/yKg2FuZ2MT0R0uc4j+W0JcRytpTXRTMCmZTVfJCnQ8fg6+CfMjrbWr9EjuOWjL58v5nIr9tHL73MqKQIWkCoD8uChlzNiCsjs2veS5U32Wy/jawvjNqsh4M+fMzorBzJNN2AkqlxG52GFFwhX2d0thvLB97qTF/z5ZTClht+zLa2ZXJ8v3oRs0ws/WBxZhAynzPaSE42/NQB9qNyePP+aMtWLYq/wehIzv5mMNiXfXlmP0y7z2g1lev/QsT+rjn7lNfUIAhpSoybRtVflrowmtk9aeTmyJVWBzdvj+1z+C2JNoOyKH5Pu9rB9aas4DsaEybZvDZ+EoFkzN8p1IB9EWdCFWg2cXp1GIdFHaetBsHYcNWc2o+28Dd2SldtOWofVy2eDirC/dk9arFaLlZNlbIFlsTpxvZxDwPyy61rM16ctJ+ake29u752MLCVfbObhk+mzVtuEuXzIN1XS77+y1Db5WqTtMpYMq8vzlap/WlZH28bo0Pejr+RPg5rX4auUSyKR5ZkPRK2csxGKlDulvWj/ZBU0+R66e7cPYX9up43Wm6HNLkVp6/ZPw0oS+9VfcErJyrcb7e73dlT9IRH5OemXduzp4DXgICAgICAgICAgICAgICAgICAgICAgICAgE/wPwCZ9lGmV0kwAAAAAElFTkSuQmCC"
            alt="Logo"
          />
          <small className="ml-3 mt-3 d-block">
            Hi there, let's begin our journey.
          </small>
        </div>
        <div className="row align-items-end px-5 pt-0 pb-0 mx-0">
          <div className="col-6 px-0">
            <img
              className="login-img d-none d-md-block"
              src={LoginPeople}
              alt="working"
              width="30%"
            />
          </div>
          <div
            className="col-md-6 col-lg-3 align-self-center pl-0 pr-0 pr-md-0"
            style={{ maxWidth: "350px" }}
          >
            <form onSubmit={(e) => this.onLogin(e)}>
              <div className="card login-card-radius login-box-shadow border-0 p-5 margin-xl">
                <h6 className="text-primary font-weight-bold mb-4">Login</h6>
                <div
                  className={classnames("input-container mt-2", {
                    "invalid-form": alert === "email" || alert === "invalid",
                  })}
                >
                  <input
                    type="text"
                    id="email"
                    name="email"
                    value={email}
                    onChange={(e) => this.setState({ email: e.target.value })}
                    placeholder=" "
                  />
                  <label htmlFor="email">Email</label>
                </div>
                <div
                  className={classnames("input-container mt-2 mb-2", {
                    "invalid-form": alert === "password" || alert === "invalid",
                  })}
                >
                  <input
                    type="password"
                    placeholder=" "
                    id="password"
                    name="password"
                    value={password}
                    onChange={(e) =>
                      this.setState({ password: e.target.value })
                    }
                  />
                  <label>Password</label>
                </div>
                <a href="#" className="text-right text-primary">
                  <small>I forgot my password</small>
                </a>
                {!isEmpty(errors) ? (
                  <div className="error-feedback">{errors}</div>
                ) : (
                  ""
                )}
                {!isEmpty(errors.password) ? (
                  <div className="error-feedback">{errors.password}</div>
                ) : (
                  ""
                )}
              </div>
              <div className="mx-2 my-4">
                <button
                  className="btn btn-primary login-card-radius w-100"
                  type="submit"
                >
                  <h6 className="font-weight-bold py-2 mb-0">Login</h6>
                </button>
              </div>
              <div className="text-center">
                <small className="mb-0">I don't have Stendard account</small>
                <a href="#">
                  <small className="d-block text-primary font-weight-bold">
                    Sign Me Up!
                  </small>
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginLayout;

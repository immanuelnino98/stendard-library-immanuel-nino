import React, { Component } from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import Login from './components/LoginPages/LoginLayout'
import LibraryLayout from './components/LibraryPages/LibraryLayout'

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/Library" component={LibraryLayout} />
          </Switch>
        </Router>
    )
  }
}

export default App